FROM debian:testing-slim

LABEL maintainer "martin.gerhardy@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y file
